function waterFloat(elm,pos,t,d,v){
var i = elm;
	
	var runIt = function (elm,pos,t,d,v) {
		if(pos == 'left'){
			
			elm.animate({right:'+='+v},t,"linear",function(){

				$({deg: -d}).animate({deg: d}, {
					duration: t,
					step: function(now){
						elm.css({
							transform: "rotate(" + now + "deg)"
						});
					}
				},"linear");

				elm.animate({right:'-='+v},t,"linear",function(){
					$({deg: d}).animate({deg: -d}, {
					duration: t,
					step: function(now){
					elm.css({
						transform: "rotate(" + now + "deg)"
						});
					}
				},"linear");
					runIt(elm,pos,t,d,v);
				});	
				
			});
		}else{
			elm.animate({top:'+='+v},t,"linear",function(){
				$({deg: -d}).animate({deg: d}, {
					duration: t,
					step: function(now){
						elm.css({
							transform: "rotate(" + now + "deg)"
						});
					}
				},"linear");
				elm.animate({top:'-='+v},t,"linear",function(){
					$({deg: d}).animate({deg: -d}, {
					duration: t,
					step: function(now){
					elm.css({
						transform: "rotate(" + now + "deg)"
						});
					}
				},"linear");
					runIt(elm,pos,t,d,v);
				});	
			});
		}
		
	}
 

 

runIt(i,pos,t,d,v);
}
